# https://adventofcode.com/2022/day/1

# Using readlines()
file1 = open('file1.txt', 'r')
Lines = file1.readlines()

count = 0
maxi = []
# Strips the newline character
for line in Lines:
    if line == '\n':
        maxi.append(int(count))
        count = 0
    else:
        count += int(line)

maxi.sort()
print(maxi[-1] + maxi[-2] + maxi[-3])