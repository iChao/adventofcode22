# https://adventofcode.com/2022/day/1

# Using readlines()
file1 = open('file1.txt', 'r')
Lines = file1.readlines()

count = 0
maxi = 0

# Strips the newline character
for line in Lines:
    if line == '\n':
        maxi = max(maxi, count)
        count = 0
    else:
        count += int(line)

maxi = max(maxi, count)
print(maxi)