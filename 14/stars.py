# https://adventofcode.com/2022/day/14

from collections import Counter
import copy

file1 = open('file.txt', 'r')
lines = file1.readlines()


def parser():
    listPoints = []
    for line in lines:
        wall = []
        for coords in line.split("->"):
            wall += ([(int(coords.split(",")[0]), int(coords.split(",")[1]))])
        completeWall = []
        completeWall += wall
        for index in range(len(wall)-1):
            if wall[index][0] == wall[index+1][0]:
                for i in range(min(wall[index][1],wall[index+1][1]),max(wall[index][1],wall[index+1][1])):
                    completeWall.append((wall[index][0],i))
            if wall[index][1] == wall[index+1][1]:
                for i in range(min(wall[index][0],wall[index+1][0]),max(wall[index][0],wall[index+1][0])):
                    completeWall.append((i, wall[index][1]))
        listPoints += completeWall
    listPoints += wall
    return listPoints

listPoints = parser()

print(listPoints)
print([(498, 4)])

DEFAULTX = 500
DEFAULTY = 0
minx, miny = min(listPoints, key=lambda tup: tup[0])[0], min(listPoints, key=lambda tup: tup[1])[1]
maxx, maxy = max(listPoints, key=lambda tup: tup[0])[0], max(listPoints, key=lambda tup: tup[1])[1]
print(minx, miny, maxx, maxy)

def printGame(array):
    for y in range(0,maxy+3):
        for x in range(minx-10,maxx+20):
            if (x,y) in array:
                print("#", end='')
            elif (x,y) == (DEFAULTX,DEFAULTY):
                print("+", end='')
            else:
                print(".", end='')
        print("\n")

def big_rain(iteration,listPoints):
    for i in range(iteration):
        x = DEFAULTX
        listPoints = rain(DEFAULTX, DEFAULTY, listPoints)
        if(listPoints == False): return i
                
def rain(x,j,listPoints):
    if(x < minx or x > maxx or j> maxy):
        return False
    for y in range(j,maxy+1):
        if (x,y+1) not in listPoints:
            return rain(x,y+1,listPoints)
        if (x-1,y+1) not in listPoints:
            return rain(x-1,y+1,listPoints)
        if (x+1,y+1) not in listPoints:
            return rain(x+1,y+1,listPoints)
        
        listPoints += [(x,y)]
        return listPoints

def big_rain2(iteration,listPoints):
    for i in range(iteration):
        if(i%1000 == 0):
            print(i)
            printGame(listPoints)
        listPoints = rain2(DEFAULTX, DEFAULTY, listPoints)
        #print(listPoints)
        if((DEFAULTX,DEFAULTY) in listPoints): 
            return i
                
def rain2(x,j,listPoints):
    if(j> maxy):
        listPoints += [(x,j)]
        return listPoints
    for y in range(j,maxy):
        if (x,y+1) not in listPoints:
            return rain2(x,y+1,listPoints)
        if (x-1,y+1) not in listPoints:
            return rain2(x-1,y+1,listPoints)
        if (x+1,y+1) not in listPoints:
            return rain2(x+1,y+1,listPoints)
 

print(big_rain2(60000,parser())+1)
