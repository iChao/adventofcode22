# https://adventofcode.com/2022/day/3
from operator import getitem
# Using readlines()
file1 = open('file1.txt', 'r')
Lines = file1.readlines()
result = []

def prioritize(item):
    if 'a' <= item <= 'z':
        return ord(item) - ord('a') + 1
    if 'A' <= item <= 'Z':
        return ord(item) - ord('A') + 27
    return 0

for i in range(0,len(Lines), 3):
    for element in Lines[int(i)]:
        if(element in Lines[i+1] and element in Lines[i+2]):
            result.append(element)
            break

total = sum(prioritize(i) for i in result)

print(total)
