# https://adventofcode.com/2022/day/3
from operator import getitem
# Using readlines()
file1 = open('file1.txt', 'r')
Lines = file1.readlines()
result = []

def prioritize(item):
    if 'a' <= item <= 'z':
        return ord(item) - ord('a') + 1
    if 'A' <= item <= 'Z':
        return ord(item) - ord('A') + 27
    return 0

for line in Lines:
    line = line.strip()
    rucksack = [line[:int(len(line)/2)], line[int(len(line)/2):]]
    for i in rucksack[0]:
        if(i in rucksack[1]):
            result.append(i)
            break
total = sum(prioritize(i) for i in result)

print(total)
