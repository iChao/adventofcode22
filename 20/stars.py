# https://adventofcode.com/2022/day/20

import logging
import re
file1 = open('file.txt', 'r')
lines = file1.readlines()
logging.basicConfig(encoding='utf-8', level=logging.WARNING)

def parsing(multiplicator):
    data = []
    for indexLine, line in enumerate(lines):
        values = [(indexLine,(int(s)*multiplicator)) for s in re.findall(r'-?\d+', line)]
        data+= values
    return data


def set_array_to_value(a, value):
    index = [tup[1] for tup in a].index(value)
    return a[index: len(a)] + a[0: index]

def mixing_sequence(data : tuple):
    string = [x[1] for x in data]
    logging.info('Initial arrangement:\n' + str(string)[1:-1]+ '\n')
    for i in range(0,len(data)):
        index = [tup[0] for tup in data].index(i) 
        val = data[index]
        new_index = (index + val[1])  % (len(data)-1)
        if(new_index <= 0): new_index= (new_index + int(new_index/len(data)-1)) % len(data)

        if(index != new_index):
            logging.info(str(data[index][1]) + ' moves between ' + str(data[new_index%len(data)][1]) + " and " + str(data[(new_index+1)%len(data)][1]))
            data.remove(val)
            data.insert(new_index % len(data), val)
        else:
            logging.info(str(data[index][1])+' does not move')
        logging.info(str([x[1] for x in data])[1:-1])
    return data

def star1():
    result = mixing_sequence(parsing(1))

    result = set_array_to_value(result,0)
    logging.info(str([x[1] for x in result])[1:-1])
    logging.info(str(result[1000%len(result)][1]), str(result[2000%len(result)][1]),str(result[3000%len(result)][1]))
    print(result[1000%len(result)][1] + result[2000%len(result)][1] + result[3000%len(result)][1])


def star2():
    result = parsing(811589153)
    for i in range(10):
        result = mixing_sequence(result)
    result = set_array_to_value(result,0)
    logging.info(str([x[1] for x in result])[1:-1])
    logging.info(str(result[1000%len(result)][1]), str(result[2000%len(result)][1]),str(result[3000%len(result)][1]))
    print(result[1000%len(result)][1] + result[2000%len(result)][1] + result[3000%len(result)][1])

star1()
star2()
