# https://adventofcode.com/2022/day/7

from collections import Counter
import copy

file1 = open('file.txt', 'r')
lines = file1.readlines()

array = {}
finalarray = {}
def parser():
    position = "/"
    for line in lines[1:]:
        line = line.strip()
        print(line, position)
        if("$ cd" in line):
            if(".." in line):
                position = position.rsplit("/", 2)[0] + "/"
                print(position)
            else:
                position +=   line[5:] + "/"
                print(position)
        if("$" not in line and "dir" not in line[:4]):

            if(position in array):
                array[position] = array[position] + int(line.split(" ")[0])
            else:
                array[position] = int(line.split(" ")[0])
            
    #print(array)
    finalarray = copy.deepcopy(array)
    for element in array:
        for i in array:
            if(element != i and i in element):
                finalarray[i] += array[element]
    return finalarray

def sumDirectories(array, size):
    sum = 0
    for element in array:
        if(int(array[element]) <= size):
            sum += int(array[element])
    print(sum)

finalarray = parser()
print(finalarray)
sumDirectories(finalarray, 100000)

#847705
