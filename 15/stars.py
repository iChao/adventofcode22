# https://adventofcode.com/2022/day/13

import logging
import re
file1 = open('file.txt', 'r')
lines = file1.readlines()
logging.basicConfig(encoding='utf-8', level=logging.INFO)

def parsing():
    data = []

    for indexLine, line in enumerate(lines):
        values = [int(s) for s in re.findall(r'-?\d+', line)]

        data.append(values)       
    return data

def manhattan(point1, point2):
    return abs(point1[0]-point2[0])+abs(point1[1]-point2[1])

def beacon_zoning(data):
    result = [['.' for x in range(0, data[3]-data[1]+1)] for y in range(0,data[4]-data[2]+1)] 
    init_y = -(data[2])
    init_x = -(data[1])
    for values in data[0]:
        print(values)
        result[values[1]+init_y][values[0]+init_x] = "S"
        result[values[3]+init_y][values[2]+init_x] = "B"
        distance = manhattan(values)
        min_x = values[0]+init_x - distance 
        min_y = values[1]+init_y - distance 
        max_x = values[0]+init_x + distance 
        max_y = values[1]+init_y + distance
        for line in range(min_y, max_y+1):
            for column in range(min_x, max_x+1):
                if(result[line][column] == "." and manhattan([values[0]+init_x,values[1]+init_y,column,line])<=distance):
                    result[line][column] = "#"

    return result

def Thales_algo(A, B, line, list):
    #triangle A : Signal, BC : Line balise, DE : Line needed
    D = [A[0],line]
    d_AB = manhattan(A, B)
    d_AD = manhattan(A, D)    
    my_range = d_AB - d_AD
    for x in range(-my_range,my_range+1):
        list[A[0]+x]= True
    return list


def Thales_algo2(A, B, line, list):
    #triangle A : Signal, BC : Line balise, DE : Line needed
    D = [A[0],line]
    d_AB = manhattan(A, B)
    d_AD = manhattan(A, D)    
    my_range = d_AB - d_AD + 1
    for x in range(-my_range,my_range+1):
        list[A[0]+x]= True
    return list

y = 2000000
result = {}
balise_on_line = {}
for element in parsing():
    if(element[3] == y):
        balise_on_line[element[2]] = True
    result = Thales_algo([element[0],element[1]], [element[2],element[3]], y, result)
print(len(result) - len(balise_on_line))

data = parsing()
for y in range(4000000):
    result = {}
    result2 = {}
    balise_on_line = {}
    if(y%10 == 0): print(y)
    for element in data:
        if(element[3] == y):
            balise_on_line[element[2]] = True
        result = Thales_algo([element[0],element[1]], [element[2],element[3]], y, result)
        result2 = Thales_algo2([element[0],element[1]], [element[2],element[3]], y, result2)
    if(len(result2) - len(balise_on_line) - (len(result) - len(balise_on_line)) > 2):
        set1 = set(result)
        set2 = set(result2)
        print(y, set2 - set1)
