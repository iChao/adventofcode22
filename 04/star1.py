# https://adventofcode.com/2022/day/4

# Using readlines()
file1 = open('file1.txt', 'r')
Lines = file1.readlines()

count = 0

# Strips the newline character
for line in Lines:
    part1, part2 = line.split(",")
    min1, max1 = part1.split("-")
    min2, max2 = part2.split("-")
    min1 = int(min1)
    min2 = int(min2)
    max1 = int(max1)
    max2 = int(max2)
    if((min1 <= min2 and max1 >= max2) or (min2 <= min1 and max2 >= max1)):
        count += 1
print(count)


