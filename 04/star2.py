# https://adventofcode.com/2022/day/4

# Using readlines()
file1 = open('file1.txt', 'r')
Lines = file1.readlines()

count = 0

for line in Lines:
    part1, part2 = line.split(",")
    min1, max1 = part1.split("-")
    min2, max2 = part2.split("-")
    min1 = int(min1)
    min2 = int(min2)
    max1 = int(max1)
    max2 = int(max2)
    if(min1 <= min2 <= max1 or min2 <= min1 <= max2):
        count += 1
    else:
        print(count, "//", min1, min2, max2, max1)
print(count)


