# https://adventofcode.com/2022/day/6

from collections import Counter

file1 = open('file.txt', 'r')
lines = file1.readlines()


visible = ([[False]*len(lines[0].strip()) for i in range(len(lines))])

def getSize():
    return sum(int(element) for element in [''.join(filter(str.isdigit, line)) for line in lines])+1

def setup_array(size):
    return ([[0]*size for i in range(size)])

def moveTail():
    maxValue = len(array)-1
    i = getSize()
    j = getSize()

    tx= i
    ty = j
    array[j][i] = 1
    for line in lines:
        print(line.strip(), "||",j,i, "||", ty,tx)
        position, nb = line.strip().split(" ")
        match position:
            case 'R':
                a = 0
                for a in range(1,int(nb)+1):
                    val = min(i+a,maxValue)
                    print(j,i,val,tx,ty)
                    if(abs(tx-val)>1):
                        ty = j
                        tx = val-1
                        array[ty][tx] = 1
                i = min(i+a,maxValue)
            case 'L':
                a = 0
                for a in range(1,int(nb)+1):
                    val = max(i-a,0)
                    print(j,i,val,tx,ty)
                    if(abs(tx-val)>1):
                        ty = j
                        tx = val+1
                        array[ty][tx] = 1
                i = max(i-a,0)
            case 'D':
                a = 0 
                for a in range(0,int(nb)+1):
                    val = min(j+a,maxValue)
                    if(abs(ty-val)>1):
                        ty = val-1
                        tx = i
                        array[ty][tx] = 1
                j = min(j+a,maxValue)
            case 'U': 
                a = 0
                for a in range(0,int(nb)+1):
                    val = max(j-a,0)
                    if(abs(ty-val)>1):
                        ty = val+1
                        tx = i
                        array[ty][tx] = 1
                j = max(j-a,0)
    return array
#visible = quadcopter_tree(array, visible)
array = setup_array(getSize()*4)

array = moveTail()

print(sum(x.count(1) for x in array))

#396 // 437 // 5289 // 5290 // 5619