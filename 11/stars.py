# https://adventofcode.com/2022/day/6

import re
import math
file1 = open('file.txt', 'r')
lines = file1.readlines()

monkeys = {}
data = []
inspect = {}

def monkeyThrow(monkedId, monkeys, number : int, symbole, condition, newMonkeyIfTrue, newMonkeyIfFalse,star):
    #print("   Monkey", monkedId, monkeys)
    for item in monkeys[monkedId]:
        if(monkedId in inspect):
            inspect[monkedId]+=1
        else:
            inspect[monkedId]=1
        #print("Monkey inspects an item with a worry level of", item)
        if(number[monkedId] == []):
            value = item
        else:
            value = number[monkedId][0]
        match symbole[monkedId]:
            case "*":
                worrylevel = item * value
                #print("Worry level is multiplied by", value, "to", worrylevel)
            case "/":
                worrylevel = item / value
                #print("Worry level is divised by", value, "to", worrylevel)
            case "+":
                worrylevel = item + value
                #print("Worry level is adeed by", value, "to", worrylevel)
            case "-":
                worrylevel = item - value
                #print("Worry level is substrate by", value, "to", worrylevel)

            case other:
                print(symbole[monkedId])
        if(star == 1):
            worrylevel = int(worrylevel/3)
        else:
            worrylevel = worrylevel%math.prod(condition)
        #print("Monkey gets bored with item. Worry level is divided by 3 to", worrylevel)
        if(worrylevel%condition[monkedId] == 0):
            throwTo = newMonkeyIfTrue[monkedId]
            #print("Current worry level is divisible by", condition)
        else:
            throwTo = newMonkeyIfFalse[monkedId]
            #print("Current worry level is not divisible by", condition)
        if throwTo in monkeys:
            monkeys[throwTo].append(worrylevel)
        else:
            monkeys[throwTo] = [worrylevel]
        #print("Item with worry level", worrylevel, "is thrown to monkey", throwTo)
    monkeys[monkedId] = []


symbole= []
number = []
condition = []
newMonkeyIfTrue = []
newMonkeyIfFalse = []
for i, line in enumerate(lines):
    if(i%7 == 0):
        pass
    if(i%7 == 1):
        value = ([int(x) for x in re.findall(r'\d+', lines[i])])
        if int(i/7) in monkeys:
            monkeys[int(i/7)] += value
        else:
            monkeys[int(i/7)] = value
    if(i%7 == 2):
        symbole.append(line[23])
        number.append([int(x) for x in re.findall(r'\d+', lines[i])])
    if(i%7 == 3):
        condition.append([int(x) for x in re.findall(r'\d+', lines[i])][0])
    if(i%7 == 4):
        newMonkeyIfTrue.append([int(x) for x in re.findall(r'\d+', lines[i])][0])
    if(i%7 == 5):
        newMonkeyIfFalse.append([int(x) for x in re.findall(r'\d+', lines[i])][0])

M = 20
for i in range(M):
    for m, monkey in enumerate(monkeys):
        monkeyThrow(m, monkeys, number, symbole, condition, newMonkeyIfTrue, newMonkeyIfFalse, 1)

print(sorted(inspect.items(), key=lambda t: t[1])[-1][1] * sorted(inspect.items(), key=lambda t: t[1])[-2][1])

M = 10000
for i in range(M):
    for m, monkey in enumerate(monkeys):
        monkeyThrow(m, monkeys, number, symbole, condition, newMonkeyIfTrue, newMonkeyIfFalse, 2)

print(sorted(inspect.items(), key=lambda t: t[1])[-1][1] * sorted(inspect.items(), key=lambda t: t[1])[-2][1])
#396 // 437 // 5289 // 5290 // 5619