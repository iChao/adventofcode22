# https://adventofcode.com/2022/day/6

import re
import math

file1 = open('file.txt', 'r')
lines = file1.readlines()

SIGNAL = [1]

for line in lines:
    if "noop" in line:
        SIGNAL.append(0)
    else:
        SIGNAL.append(0)
        SIGNAL.append(([int(x) for x in re.findall(r'-?\d+', line)][0]))

def firstStar(signal):
    V = 0
    score = 0
    for cycle in range(len(signal)):
        if(cycle%40 == 20):
            score += V * cycle 
            print(cycle, V, score)
        V += signal.pop(0)

def secondStar(signal):
    V = 0
    score = 0
    signal.pop(0)
    for cycle in range(len(signal)):
        if(cycle <239 and signal[1] != 0):
            print("Start cycle   ",cycle+1,":  begin executing addx ",signal[1])
        print("During cycle ",cycle+1,": CRT draws pixel in position",cycle)
        print("Current CRT row: ",)
        if(cycle>5):
            break
        if(cycle%40 == 20):
            score += V * cycle 
            print(cycle, V, score)
        V += signal.pop(0)
        if(signal[0] !=0):
            print("End of cycle  ",cycle+1,": finish executing addx",signal[0], "(Register X is now",V,")))")
            print("Sprite position :", ("".join('.' for i in range(10))))

firstStar(SIGNAL.copy())
secondStar(SIGNAL.copy())