# https://adventofcode.com/2022/day/5

# Using readlines()
file1 = open('file1.txt', 'r')
Lines = file1.readlines()

column = int(len(Lines[0])/4)

for rows, line in enumerate(Lines):
    if("1" in line):
        break

data = Lines[0:rows]
array = [[0 for x in range(rows)] for y in range(column)]

for i,line in enumerate(data):
    for element in range(column):
        array[element][-i-1] = line[1+element*4]

def remove_values_from_list(the_list, val):
    return [value for value in the_list if value != val]

for i, element in enumerate(array):
    array[i] = remove_values_from_list(element, " ")

for line in Lines[rows+2:]:
    nb = [int(s) for s in line.split() if s.isdigit()]
    poplist = []
    for i in range(nb[0]):
        poplist.append(array[nb[1]-1].pop())
    for i in reversed(poplist):
        array[nb[2]-1].append(i)

string = ""
for element in array:
    string += element[-1]

print(string)
