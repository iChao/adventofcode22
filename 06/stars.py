# https://adventofcode.com/2022/day/6

from collections import Counter

file1 = open('file.txt', 'r')
line = file1.readlines()[0]

def occurence_max_is_1(array):
    for i in range(len(array)):
        if(Counter(array)[array[i]] != 1):
            return False
    return True

def setup_array(size):
    array = []
    for i in range(size):
        array.append(line[i])
    return array

def find_marker(size):
    array = setup_array(size)
    for index in range(len(array),len(line)):
        if(not(occurence_max_is_1(array))):
            array.pop(0)
            array.append(line[index])
        else:
            break
    print(array, index)

find_marker(4)
find_marker(14)