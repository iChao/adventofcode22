# https://adventofcode.com/2022/day/13

import logging
file1 = open('file.txt', 'r')
lines = file1.readlines()
logging.basicConfig(encoding='utf-8', level=logging.INFO)

def parsing():
    data = []
    for indexLine, line in enumerate(lines):
        nbOpen = []
        currentData = []
        currentNumber = ""
        logging.info('I told you so')
        for indexChar, char in enumerate(line):
            currentData = data
            for i in range(0,len(nbOpen)):
                currentData= currentData[-1]
            if(char == '['):
                nbOpen.append(indexChar)
                currentData.append([])
            if(char == ']'):
                nbOpen.pop()
                if(currentNumber != ""):
                    currentData.append(int(currentNumber))
                    currentNumber = ""
            if(char == ','):
                if(currentNumber != ""):
                    currentData.append(int(currentNumber))
                    currentNumber = ""
            if( '0' <= char <= '9'):
                currentNumber += char
    return data
        


star1 = parsing()
print(star1)

for index in range(0,len(star1),2):
    print(star1[index], "VERSUS", star1[index+1])