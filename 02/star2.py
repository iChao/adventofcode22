# https://adventofcode.com/2022/day/2

# Using readlines()
file1 = open('file2.txt', 'r')
Lines = file1.readlines()

score = 0
finalscore = 0
# Strips the newline character
for line in Lines:
    a,b = line.replace("\n", "").split(" ")
    score = (ord(a) + ord(b)-ord("Y")-ord('A')) %3 +1
    if ord(b)== ord("Y"):
        score += 3
    if ord(b) == ord("Z"):
        score += 6
    finalscore += score

print(finalscore)
