# https://adventofcode.com/2022/day/2

# Using readlines()
file1 = open('file2.txt', 'r')
Lines = file1.readlines()

score = 0
finalscore = 0
# Strips the newline character
for line in Lines:
    a,b = line.replace("\n", "").split(" ")
    score = ord(b)- ord("W")
    bonus = (ord(a)- ord(b) + ord("X") - ord("A")) %3
    if bonus== 2:
        score += 6
    if bonus == 0:
        score += 3
    finalscore += score

print(finalscore)
